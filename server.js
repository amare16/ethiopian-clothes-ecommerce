const express = require('express');
const ImportData = require('./DataImport');
const productRoute = require('./Routes/productRoute');
const userRoute = require('./Routes/userRoute');
const orderRoute = require('./Routes/orderRoute');

require('dotenv').config({path: './config/.env'});
require('./config/database');
const { notFound, errorHandler } = require('./middleware/Errors');


const app = express();
app.use(express.json());

// API
app.use("/api/import", ImportData);
app.use("/api/products", productRoute);
app.use("/api/users", userRoute);
app.use("/api/orders", orderRoute);

// ERROR HANDLER
app.use(notFound);
app.use(errorHandler);


const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`Server is running in port ${PORT}`));