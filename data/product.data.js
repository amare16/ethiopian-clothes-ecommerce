const products = [
    {
      name: "Zagwe Habesha Dress",
      image: "/images/6.png",
      description:
        "Handwoven and hand embroidered, Hand wash/dry cleaning",
      price: 262,
      countInStock: 3,
      rating: 4,
      numReviews: 5,
    },
    {
      name: "Tiringo Ethiopian Dress",
      image: "/images/5.png",
      description:
        "Ethiopian dress for wedding, church, Handwoven by traditional Ethiopian weavers (Shemane), Hand wash/Dry cleaning",
      price: 280,
      countInStock: 10,
      rating: 2,
      numReviews: 2,
    },
    {
      name: "Lemlem Habesh dress",
      image: "/images/4.png",
      description:
        "Lemlem Ethiopian Traditional cloth, Handwoven cotton fabric, hand wash or dry cleaning",
      price: 215,
      countInStock: 0,
      rating: 3.5,
      numReviews: 3,
    },
    {
      name: "Konjo Ethiopian traditional cloth",
      image: "/images/3.png",
      description:
        " Ethiopian traditional cloth, Handwoven by traditional Ethiopian weavers (Shemane), Hand wash/Dry cleaning",
      price: 377,
      countInStock: 10,
      rating: 5,
      numReviews: 9,
    },
    {
      name: "Asmarina Zuria",
      image: "/images/2.png",
      description:
        "Asmarina New Habesha zuria- Fits from Small to Large, Hand wash/Dry cleaning",
      price: 181,
      countInStock: 7,
      rating: 2,
      numReviews: 2,
    },
  ];
  
  module.exports = products;

  // images
  /*
  zagwe: https://cdn.shopify.com/s/files/1/2447/6309/products/E0A8714_2048x.jpg?v=1622186345
  Tringo: https://cdn.shopify.com/s/files/1/2447/6309/products/1M2A4606_2048x.jpg?v=1622188864
  Lemlem: https://cdn.shopify.com/s/files/1/2447/6309/products/1M2A1978_2048x.JPG?v=1622462345
  Konjo: https://cdn.shopify.com/s/files/1/2447/6309/products/konjo-ethiopian-traditional-cloth-eritrean-wedding-dress-habesha-weddin-dresses-clothing-white-orange-fashion-tradition-formal-wear_627_2048x.jpg?v=1622462824
  Asmarina: https://cdn.shopify.com/s/files/1/2447/6309/products/asmarina-zuria-eritrean-ethiopian-traditional-habesha_766_2048x.jpg?v=1621692547
  */