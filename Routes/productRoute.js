const express = require("express");
const asyncHandler = require("express-async-handler");
const Product = require("../models/productModel");
const protect = require("../middleware/authMiddleware");

const productRoute = express.Router();

// Get all products
productRoute.get(
  "/",
  asyncHandler(async (req, res) => {
    const products = await Product.find({});
    res.json(products);
  })
);

// Get single product
productRoute.get(
  "/:id",
  asyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id);
    if (product) {
      res.json(product);
    } else {
      res.status(404);
      throw new Error(`Product not found`);
    }
  })
);

// PRODUCT REVIEW
productRoute.post(
  "/:id/review",
  protect,
  asyncHandler(async (req, res) => {
    const { rating, comment } = req.body;
    const product = await Product.findById(req.params.id);

    if (product) {
      const productReviewedAlready = product.reviews.find(
        (r) => r.user.toString() === req.user._id.toString()
      );
      
      if (productReviewedAlready) {
        res.status(400);
        throw new Error("Product already revieed");
      }

      const review = {
        name: req.user.name,
        rating: Number(rating),
        comment,
        user: req.user._id,
      };

      product.reviews.push(review);
      product.numReviews = product.reviews.length;
      product.rating =
        product.reviews.reduce((acc, item) => item.rating + acc, 0) /
        product.reviews.length;

      await product.save();
      res.status(201).json({ message: "Product Review Added" });
    } else {
      res.status(404);
      throw new Error(`Product not found`);
    }
  })
);

module.exports = productRoute;
