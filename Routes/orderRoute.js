const express = require("express");
const asyncHandler = require("express-async-handler");
const Order = require("../models/orderModel");
const generateToken = require("../utils/generateToken");
const protect = require("../middleware/authMiddleware");

const orderRoute = express.Router();

// CREATE ORDER
orderRoute.post(
  "/",
  protect,
  asyncHandler(async (req, res) => {
    const {
      orderItems,
      shippingAddress,
      paymentMethod,
      itemsPrice,
      taxPrice,
      shippingPrice,
      totalPrice,
    } = req.body;
    

    if (orderItems && orderItems.length === 0) {
        res.status(400);
        throw new Error('No order items');
        
        return;
    } else {
        const order = new Order({
            orderItems,
            shippingAddress,
            paymentMethod,
            itemsPrice,
            taxPrice,
            shippingPrice,
            totalPrice,
        });

        const createOrder = await order.save();
        res.status(201).json(createOrder);
    }
  })
);

// GET ORDER BY ID
orderRoute.get(
    "/:id",
    protect,
    asyncHandler(async (req, res) => {
      const order = await Order.findById(req.params.id).populate(
          "user",
          "name email"
      );      
  
      if (order) {
          res.json(order);
      } else {
          res.status(404);
          throw new Error("Order Not Found");
      }
    })
  );

  // ORDER IS PAID
orderRoute.put(
  "/:id/paid",
  protect,
  asyncHandler(async (req, res) => {
    const order = await Order.findById(req.params.id);      

    if (order) {
      order.isPaid = true;
      order.paidAt = Date.now();
      order.paymentResult = {
        id: req.body.id,
        status: req.body.status,
        update_time: req.body.update_time,
        email_address: req.body.email_address,
      };

      const updatedOrder = await order.save();
      res.json(updatedOrder);
    } else {
        res.status(404);
        throw new Error("Order Not Found");
    }
  })
);

module.exports = orderRoute;
