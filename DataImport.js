const express = require("express");
const User = require("./models/userModel");
const Product = require("./models/productModel");
const users = require("./data/user.data");
const products = require("./data/product.data");
const asyncHandler = require("express-async-handler");

const ImportData = express.Router();

ImportData.post(
  "/user",
  asyncHandler(async (req, res) => {
    await User.remove({});

    const importUser = await User.insertMany(users);
    res.send({ importUser });
  })
);

ImportData.post(
  "/products",
  asyncHandler(async (req, res) => {
    await Product.remove({});

    const importProduct = await Product.insertMany(products);
    res.send({ importProduct });
  })
);

module.exports = ImportData;
